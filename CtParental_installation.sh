#!/usr/bin/env bash


Ctparental_URL="https://gitlab.com/marsat/CTparental/uploads/1e7084ac9d1aa19bd27a97652c9e6eb7/ctparental_ubuntu16.04_lighttpd_4.44.05-1.0_all.deb"

# Installateur bash pour Ctparental
# Compte enfants doivent déja être fait pour faciliter la configuration, possible 

# Mise à jour du système
	sudo apt-get update && sudo apt upgrade -y

# Création d'un dossier temporaire
	mkdir /tmp/Ctparental

# Déplacement vers le dossier temporaire
	cd /tmp/Ctparental

# Téléchargement du paquet CtParental
	wget $Ctparental_URL

# Dépaquetage et tentative d'installation, échouera sauf si dépendence présente
	sudo dpkg -i  ctparental_*.deb

# Installation des dépendances si la commande précédente a échouée
	if [[ $? != 0 ]]; then
		sudo apt install -y
		sudo dpkg -i  ctparental_*.deb
	fi
	